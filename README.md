This script was created by Micah Lee. Credits to him. Affected versions 12.04 - 12.10 - 13.04 and 13.10.

I Only copied the content of the script and put it into my personal repos on github in a shell script executable file for personal use and to have a way to find it most faster, but, you can use it as well without any problems.

Site: https://fixubuntu.com/ # Official Site of the Script

To install this script into your system:

wget -q -O - https://fixubuntu.com/fixubuntu.sh | bash

If you not have wget in your system, you need to install it:

On Ubuntu/Debian and Linux Mint:

sudo apt-get install wget

Licensed under AGPLv3.
